#include "RenderingGame.h"
#include "GameException.h"
#include "FirstPersonCamera.h"
#include "TriangleDemo.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "ModelFromFile.h"
#include "FpsComponent.h"
#include "RenderStateHelper.h"

namespace Rendering
{;

	const XMFLOAT4 RenderingGame::BackgroundColor = { 0.7f, 1.5f, 1.8f, 1.0f };		// background color

    RenderingGame::RenderingGame(HINSTANCE instance, const std::wstring& windowClass, const std::wstring& windowTitle, int showCommand)
        :  Game(instance, windowClass, windowTitle, showCommand),
        mDemo(nullptr), mModel(nullptr), mMouse(nullptr), mKeyboard (nullptr), mDirectInput (nullptr), mFpsComponent(nullptr), mRenderStateHelper(nullptr),
		mTestModel(nullptr)
    {
        mDepthStencilBufferEnabled = true;
        mMultiSamplingEnabled = true;

	
    }

    RenderingGame::~RenderingGame()
    {

    }

    void RenderingGame::Initialize()
    {
		

        mCamera = new FirstPersonCamera(*this);
        mComponents.push_back(mCamera);
        mServices.AddService(Camera::TypeIdClass(), mCamera); 
    
        mDemo = new TriangleDemo(*this, *mCamera);
        mComponents.push_back(mDemo);

		#pragma region Input
		if (FAILED(DirectInput8Create(mInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (LPVOID*)&mDirectInput, nullptr)))
		{
			throw GameException("DirectInput8Create() failed");
		}

		mFpsComponent = new FpsComponent(*this);
		mFpsComponent->Initialize();
		mRenderStateHelper = new RenderStateHelper(*this);

		mKeyboard = new Keyboard(*this, mDirectInput);
		mComponents.push_back(mKeyboard);
		mServices.AddService(Keyboard::TypeIdClass(), mKeyboard);

		mMouse = new Mouse(*this, mDirectInput);
		mComponents.push_back(mMouse);
		mServices.AddService(Mouse::TypeIdClass(), mMouse);

		mModel = new ModelFromFile(*this, *mCamera, "Content\\Models\\bench.3ds");	
		mModel->SetPosition(-1.57f, -0.0f, -0.0f, 0.005f, 0.0f, 0.6f, 4.0f);
		mComponents.push_back(mModel);
		
		mTestModel = new ModelFromFile(*this, *mCamera, "Content\\Models\\ModelTestwithMAP.3ds");
		mTestModel->SetPosition(-1.57f, -0.0f, -0.0f, 0.5f, 0.0f, 0.6f, 4.0f);
								// 1. rotateX, 2. rotateY, 3. rotateZ, 4. scale, 5. translateX
								// 6. translateY, 7. translateZ
		mComponents.push_back(mTestModel);
		
		#pragma endregion  

        Game::Initialize();

		


		mCamera->SetPosition(0.0f, 0.0f, 5.0f);
    }

    void RenderingGame::Shutdown()
    {
		
		DeleteObject(mDemo);
		DeleteObject(mModel);
        DeleteObject(mCamera);
		DeleteObject(mKeyboard);
		DeleteObject(mMouse);
		DeleteObject(mTestModel);
		ReleaseObject(mDirectInput);
		//ReleaseObject(mFpsComponent);
		//ReleaseObject(mRenderStateHelper);

        Game::Shutdown();
    }

    void RenderingGame::Update(const GameTime &gameTime)
    {
		// add for camera

		mFpsComponent->Update(gameTime);

		if (mKeyboard->WasKeyPressedThisFrame(DIK_ESCAPE)) {

					Exit();
		}

		
        Game::Update(gameTime);
    }

    void RenderingGame::Draw(const GameTime &gameTime)
    {
		mRenderStateHelper->SaveAll();
		mFpsComponent->Draw(gameTime);
		mRenderStateHelper->RestoreAll();

        mDirect3DDeviceContext->ClearRenderTargetView(mRenderTargetView, reinterpret_cast<const float*>(&BackgroundColor));
        mDirect3DDeviceContext->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);


        Game::Draw(gameTime);
       
        HRESULT hr = mSwapChain->Present(0, 0);
        if (FAILED(hr))
        {
            throw GameException("IDXGISwapChain::Present() failed.", hr);
        }
    }
}